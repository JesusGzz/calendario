﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/js/bootstrap.bundle.min.js" integrity="sha384-qKXV1j0HvMUeCBQ+QVp7JcfGl760yU08IQ+GpUo5hlbpg51QRiuqHAJz8+BrxE/N" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous"/>
    <script src="Scripts/jquery-3.6.4.min.js"></script>
    <script src="Scripts/jquery-3.6.4.js"></script>
    <link href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css" rel="stylesheet"/>
    <script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script src="jquery-datatables-checkboxes-1.2.12/js/dataTables.checkboxes.js"></script>
    <link href="jquery-datatables-checkboxes-1.2.12/css/dataTables.checkboxes.css" rel="stylesheet" />
    <style>
        tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    </style>
</head>
<body>
    <form runat="server">
        <div class="container-fluid text-center overflow-hidden">
            <div class="row g">
                <div class="col-3 border border-5 border-dark m-4">
                    <asp:RadioButtonList id="RadioButtonList1" 
                           AutoPostBack="false"
                           CellPadding="5"
                           CellSpacing="5"
                           RepeatColumns="1"
                           RepeatDirection="Vertical"
                           RepeatLayout="Flow"
                           TextAlign="Right"
                           runat="server">
                    <asp:ListItem Value="QUICK SHINE">QUICK SHINE</asp:ListItem>
                    <asp:ListItem Value="TEC TORRES">TEC TORRES</asp:ListItem>
                    <asp:ListItem Value="ESTADIO BBVA">ESTADIO BBVA</asp:ListItem>
                    <asp:ListItem Value="H&M">H&M</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div class="col-3 border border-5 border-dark m-4">
                    <asp:CheckBoxList id="checkboxlist2" 
                           AutoPostBack="false"
                           CellPadding="5"
                           CellSpacing="5"
                           RepeatColumns="1"
                           RepeatDirection="Vertical"
                           RepeatLayout="Flow"
                           TextAlign="Right"
                           runat="server"> 
                         <asp:ListItem Value="S1">S1</asp:ListItem>
                         <asp:ListItem Value="S2">S2</asp:ListItem>
                         <asp:ListItem Value="S3">S3</asp:ListItem>
                         <asp:ListItem Value="S4">S4</asp:ListItem>
                         <asp:ListItem Value="Q1">Q1</asp:ListItem>
                         <asp:ListItem Value="Q2">Q2</asp:ListItem>
                         <asp:ListItem Value="M1">M1</asp:ListItem>
                         <asp:ListItem Value="D1">D1</asp:ListItem>
                      </asp:CheckBoxList>
                </div>
                <div id="dvCheckBoxListControl" class="col-3 border border-5 border-dark m-4">
                <asp:CheckBoxList id="checkboxlist1" 
                       AutoPostBack="false"
                       CellPadding="5"
                       CellSpacing="5"
                       RepeatColumns="1"
                       RepeatDirection="Vertical"
                       RepeatLayout="Flow"
                       TextAlign="Right"
                       runat="server"> 
                  </asp:CheckBoxList>
                </div>
            </div>
            <div class="row g">
                <div class="col">
                    <table id="example" class="display nowrap" style="width:100%">
                        <thead>
                            <tr>
                                <th>ProyectoId</th>
                                <th>Clave</th>
                                <th>Nombre  </th>
                                <th>Cadena CMK</th>
                                <th>Nombre Tienda</th>
                                <th>FRECUENCIA</th>
                                <th>CadenasId</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ProyectoId</th>
                                <th>Clave</th>
                                <th>Nombre  </th>
                                <th>Cadena CMK</th>
                                <th>Nombre Tienda</th>
                                <th>FRECUENCIA</th>
                                <th>CadenasId</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>




                
            </div>
    </form>
<div class="modal modal_backdrop" id="idLoading">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <div class="text-center">
                    <div class="spinner-border text-primary"></div>
                        <img src="loading.gif" />                    
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#example tfoot th').each(function () {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="' + title + '" />');
        });

        $("#RadioButtonList1").change(function (e) {
            $.ajax({
                type: "POST",
                url: "Default.aspx/cargarProyectos",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(response.d);
                },
                error: function (response) {
                    alert(response.d);
                }
            });
        });

        $("#RadioButtonList1").change(function (e) {
            e.preventDefault();
            var da = $("#<%= RadioButtonList1.ClientID %> input:checked").val();
            $.ajax({
                type: "POST",
                url: "Default.aspx/cargarEncuesta",
                contentType: "application/json; charset=utf-8",
                data: '{"name":"' + da + '"}',
                dataType: "json",
                success: AjaxSucceeded,
                error: function (xhr, status, error) {
                    alert("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)
                }
            });
        });
        
        $(document).ajaxStart(function () {
            $("#idLoading").modal('show');
        });

        $(document).ajaxStop(function () {
            $("#idLoading").modal('hide');
        });

        function OnSuccess(response) {
            console.log(response);
            $("[id*=example]").DataTable(
            {
                bLengthChange: true,
                lengthMenu: [[5, 10, -1], [5, 10, "All"]],
                bFilter: true,
                bSort: true,
                bPaginate: true,
                data: response.d,
                columns: [{ 'data': 'ProyectoId' },
                          { 'data': 'Clave' },
                          { 'data': 'Nombre' },
                          { 'data': 'CadenaCMK' },
                          { 'data': 'NombreTienda' },
                          { 'data': 'FRECUENCIA' },
                          { 'data': 'CadenasId' }],
                initComplete: function () {
                    // Apply the search
                    this.api()
                        .columns()
                        .every(function () {
                            var that = this;

                            $('input', this.footer()).on('keyup change clear', function () {
                                if (that.search() !== this.value) {
                                    that.search(this.value).draw();
                                }
                            });
                        });
                },
                columnDefs: [{
                    targets: 0,
                    checkboxes: {
                    selectRow: true
                }
                }],
                select: {
                    style: 'multi',
                    selector: 'td:not(:first-child)'
                    
                },
                order: [[1, 'asc']],
            })
        }

        function AjaxSucceeded(result) {
            BindCheckBoxList(result);
        }
        function AjaxFailed(result) {
            alert('Failed to load checkbox list');
        }
        function BindCheckBoxList(result) {

            var items = JSON.parse(result.d);
            console.log(items);
            CreateCheckBoxList(items);
        }
        function CreateCheckBoxList(checkboxlistItems) {
            console.log(checkboxlistItems);
            var table = $('<table></table>');
            var counter = 0;
            $(checkboxlistItems).each(function () {
                table.append($('<tr></tr>').append($('<td></td>').append($('<input>').attr({
                    type: 'checkbox', name: 'chklistitem', value: checkboxlistItems, id: 'chklistitem' + counter
                })).append(
                $('<label>').attr({
                    for: 'chklistitem' + counter++
                }).text(checkboxlistItems))));
            });

            $('#dvCheckBoxListControl').append(table);
        }
    });
</script>
</body>
</html>