﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    [WebMethod]
    public static List<string> cargarEncuesta(string name)
    {
        string query = "SELECT CadenasID FROM Cadenas WHERE Nombre = '"+name+"'";

        List<string> encuestas = new List<string>();
        SqlConnection conn = new SqlConnection();
        SqlCommand cmd = new SqlCommand();

        DataTable dt = new DataTable();
        conn.ConnectionString = ConfigurationManager.ConnectionStrings["con"].ConnectionString;
        cmd.Connection = conn;

        cmd.CommandType = CommandType.Text;
        cmd.CommandText = query;
        cmd.Connection = conn;

        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
        sqlDataAdapter.SelectCommand = cmd;

        sqlDataAdapter.Fill(dt);

        int i = 0;
        foreach (DataRow dr in dt.Rows) {
            encuestas.Add(dr["CadenasId"].ToString());
            
        }
            //stringBuilder.Append("<asp:ListItem Value=\"" + dr["CadenasId"] + "\">" + dr["CadenasId"] + "</asp:ListItem>");
        return encuestas;
    }

    [WebMethod]
    public static List<Proyectos> cargarProyectos()
    {
        List<Proyectos> proyectos = new List<Proyectos>();
        string query = "SELECT distinct [ProyectoId],[Clave],[Nombre],[Cadena CMK],[Nombre Tienda],[FRECUENCIA],[CadenasId]FROM[Collector].[dbo].[PreRelacionEncuestasProyectos]";
        SqlConnection conn = new SqlConnection();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();
        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();

        conn.ConnectionString = ConfigurationManager.ConnectionStrings["con"].ConnectionString;
        cmd.Connection = conn;
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = query;
        cmd.Connection = conn;       
        sqlDataAdapter.SelectCommand = cmd;
        sqlDataAdapter.Fill(dt);
        foreach (DataRow dr in dt.Rows)
        {
            proyectos.Add(new Proyectos() {
                ProyectoId = Convert.ToInt32(dr["ProyectoId"]),
                Clave = dr["Clave"].ToString(),
                Nombre = dr["Nombre"].ToString(),
                CadenaCMK = dr["Cadena CMK"].ToString(),
                NombreTienda = dr["Nombre Tienda"].ToString(),
                FRECUENCIA = dr["FRECUENCIA"].ToString(),
                CadenasId = Convert.ToInt32(dr["CadenasId"])
            });

        }
        return proyectos;
    }
}

public class Proyectos{
    public int ProyectoId { get; set; }
    public string Clave { get; set; }
    public string Nombre { get; set; }
    public string CadenaCMK { get; set; }
    public string NombreTienda { get; set; }
    public string FRECUENCIA { get; set; }
    public int CadenasId { get; set; }


}

//<option value=\"" + dr["ProductID"] + "\">" + dr["ProductName"] + "</option>