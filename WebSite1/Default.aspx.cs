﻿/*
Este programa pretende realizar la carga de calendarios en la base de datos y ejecutar una serie de Stored Procedures 
que aseguran una carga correcta y completa de la información recibida, con el fin de que esta información pueda ser consumida luego
por la aplicación Flex Movil y que los asesores puedan ver que visitas a clientes se les asigno.
Estas cargas son mensuales y marcan las visitas que un cliente debe recibir, con que frecuencia y en que intervalo de tiempo
debe ser realizada, tambien la acompañan datos extras que complementan la información necesaria para una visita.  
*/
// Autor: Jesus Gonzalez
//Fecha Inicial: 13/02/2023
//

using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    #region Variables Publicas
    public static string fileName; //Guarda el nombre del archivo excel que se carga
    public static string DataSource = AppDomain.CurrentDomain.BaseDirectory;//Guarda la dirección desde donde se corre el proyecto
    public static string Path;//Combina DataSource+fileName para formar la ruta completa del archivo con el que se va a trabajar
    public static string Provider = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=";
    public static string connStr;//guarda parte de la conexion para OleDb
    public static int id;//guarda el numero de id sobre el cual se comenzara la carga, 0 si es una carga nueva y si es subsecuente guarda el valor que devuelve la función VerificarId
    public static int e;//cuenta errores en excel, si e=0 carga excel en DB else regresa un excel con celdas marcadas para corregir
    public static string year1;//Guarda el dato del componente asignado a recibir el año al que pertenece el archivo que se esta cargando
    public static string year;//Guarda el dato del componente asignado a recibir el año al que pertenece el archivo que se esta cargando
    public static string month1;//Guarda el dato del componente asignado a recibir el mes al que pertenece el archivo que se esta cargando
    public static string month;//Guarda el dato del componente asignado a recibir el mes al que pertenece el archivo que se esta cargando
    public static string service1;//Guarda las iniciales de la empresa que esta cargando el archivo
    public static string service;//Guarda las iniciales de la empresa que esta cargando el archivo
    public static string project;//Comprende un grupo de datos para asignar información acerca del procedimiento que se realiza
    public static DateTime fInicial;//Guarda el dato del componente asignado a recibir la fecha inical del archivo cargado

    //Las listas siguientes en conjunto apuntan a una celda en particular, los items en ambas listas estan enparejados con sus indices, osea x[n] e y[n] son pareja
    public static List<int> x1;//Guarda una lista con los indices de las columnas que no pasaron la validacion por nulos en ValidacionExcel()
    public static List<int> y1;//Guarda una lista con los indices de las filas que no pasaron la validacion por nulos en ValidacionExcel()
    public static List<int> x2;//Guarda una lista con los indices de las columnas que no pasaron la validacion por tipos en ValidacionExcel()
    public static List<int> y2;//Guarda una lista con los indices de las filas que no pasaron la validacion por tipos en ValidacionExcel()
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
        }
    }

    #region Carga Excel

    //Ejecuta ChecarParametros(), si pasa la prueba asigna valores de componentes, guarda el archivo dentro de la carpeta del proyecto
    //crea una conexion con el archivo y lo almaneca en un objeto DataSet para poder trabajar con el
    protected void CargarExcel(object sender, EventArgs e)
    {
        id = 0;//Reestablece la variable para un uso nuevo
            //Checa que se haya cargado un archivo, si true intnta guardarlo en el path de este proyecto y lo carga dicho en un DataSet posteriormente
            if (FileUpload1.HasFile)
            {
                int save = 0;
                try
                {
                    SaveFile(FileUpload1.PostedFile);//guarda el excel en la carpeta interna Excel File para controlar el path del archivo
                    save = 1;
                }
                catch
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert", "alert('Nombre pai no se salvo :,(');", true);
                }
                if (save == 1)
                {
                    connStr = "";//Reestablece el conectionstring dque se usa en el OleDbConnection
                    connStr = Provider + Path + ";Extended Properties=\"Excel 12.0 Xml; HDR=YES\"";//Completa el path para OleDb
                    OleDbConnection OleDbcon = new OleDbConnection(connStr);//Crea la conexión al archivo excel que se cargo
                    OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Hoja1$]", OleDbcon);//Crea una consulta para usar en el archivo al que se esta conectado
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter(cmd);//Crea un adaptador que recibira toda la información de la consulta que se realizo
                    DataSet ds = new DataSet();//Se crea un objeto DataSet que contendra las tablas que vamos a manipular
                    objAdapter1.Fill(ds);//Se crea una tabla dentro de Data set con todo el contenido del adaptador
                    ValidarExcel(ds);//Se realizan validaciones sobre los datos que se obtuvieron del archivo excel que se ingreso
                    ds.Reset();
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert", "alert('Something happens ese :O,(');", true);
                    //Manda mensaje de error (no tiene que ser ese de ahiXD)
                }
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert", "alert('Falta el file pai :O,(');", true);
                //Manda mensaje de error (no tiene que ser ese de ahiXD)
            } 
        //puebla los DataGrid para visualizar como trabaja el proyecto
    }

    //Guarda el archivo que proporcionn, si ya existe uno con ese nombre borra el anterior primero para evitar duplicados
    void SaveFile(HttpPostedFile file)
    {
        fileName = file.FileName;//Guarda el nombre del archivo que se esta subiendo
        Path = DataSource + fileName;//Combina el nombre del archivo con el path de este proyecto

        //busca por un archivo con el mismo nombre, si existe lo borra para evitar duplicados y guarda el nuevo archivo en el path de este proyecto

        if (System.IO.File.Exists(Path))
        {
            try
            {
                System.IO.File.Delete(Path);
                FileUpload1.SaveAs(Path);
            }
            catch
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert", "alert('Algo fallo con la borrada pai :O');", true);
            }
        }
        else
        {
            try
            {
                FileUpload1.SaveAs(Path);
            }
            catch
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert", "alert('Algo fallo con la salvada pai :O');", true);
            }
        }
    }

    #endregion

    #region Verifica excel
    //Recibe DataSet y realiza procesos para validar por nulos y aun falta completar el de formato de fecha
    public void ValidarExcel(DataSet ds)
    {
        #region Variables y objetos
        Dictionary<string, string> tp = new Dictionary<string, string>();//un diccionario para comparar los tipos de datos de un DataTable con la BD
        tp.Add("System.String", "nvarchar");
        tp.Add("System.Double", "int");
        tp.Add("System.DateTime", "datetime");
        tp.Add("System.DBNull", "null");
        List<String> types = new List<string>();//Guarda el tipo de dato de una columna
        DataTable dt = new DataTable();//Crea objeto DataTable que almacenara los datos de la primera tabla del DataSet
        dt = ds.Tables[0];
        if (!dt.Columns.Contains("Calendario"))
        {
            DataColumn Calendario = dt.Columns.Add("Calendario", typeof(string));
            Calendario.SetOrdinal(0);
        }
        dt.Columns.Remove(dt.Columns[2]);
        DataColumn IdVisitas = dt.Columns.Add("ID Visita", typeof(Double));
        IdVisitas.SetOrdinal(2);
        DataTable tipos = new DataTable();//Crea objeto DataTable que se usara para poblarla con el tipo de dato correspondiente a cada celda de "dt
        ds.Tables.Add(tipos);//Agrega la tabla "tipos" al DataSet
        x1 = new List<int>();//Instancias limpias de la variable publica x1
        y1 = new List<int>();//Instancias limpias de la variable publica y1
        x2 = new List<int>();//Instancias limpias de la variable publica x2
        y2 = new List<int>();//Instancias limpias de la variable publica y2
        fInicial = new DateTime();
        fInicial = Convert.ToDateTime(dt.Rows[1][dt.Columns["INICIO POR PROYECTO"]]);
        year1 = fInicial.ToString("yyyy");
        year = fInicial.ToString("yy");
        month1 = dt.Rows[1][dt.Columns["ESQUEMA"]].ToString();
        month = dt.Rows[1][dt.Columns["ESQUEMA"]].ToString().Substring(0,3);
        service1 = dt.Rows[1][dt.Columns["Equipo"]].ToString();

        string s1 = "";
        string s2 = "";
        string s3 = "";
        string s4 = "";
        string equipo = dt.Rows[1][dt.Columns["Equipo"]].ToString();
        if (equipo == "")
        {
            s1 = "N";
            s2 = "A";
        }else
        {
            s1 = dt.Rows[1][dt.Columns["Equipo"]].ToString();
            s3 = s1.Substring(0, 1);
            s2 = dt.Rows[1][dt.Columns["Equipo"]].ToString();
            s4 = s2.Substring(6, 1);
        }
        service = s3 + s4;
        Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert", "alert(" +service+ ");", true);
        project = service + month + year;
        #endregion

        #region Asigna IdVisita

        id = VerificarId(project);//Obtiene el id de visita inicial para la carga del archivo
        //asigna el Id de Visita en su respectiva columna
        foreach (DataRow row in dt.Rows)
        {
            dt.Rows[dt.Rows.IndexOf(row)][2] = dt.Rows.IndexOf(row) + id;
        }
        #endregion

        #region Asigna nombre del proyecto
        string checarProyecto = project.Substring(0, 2);
        if (checarProyecto.Equals("NA"))
        {

        }
        else
        {
            foreach (DataRow row in dt.Rows)
            {
                dt.Rows[dt.Rows.IndexOf(row)][0] = project;
            }
        }
        
        #endregion

        //Puebla una lista con los tipos de datos de las columnas de la BD en el orden en el que se encuentran en la BD y los 
        #region Puebla lista con tipos de datos de columnas en BD
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString))
        {
            using (SqlCommand command = conn.CreateCommand())
            {
                //Lee los tipos de datos de las columnas
                command.CommandText = "Select DATA_TYPE from information_schema.columns WHERE TABLE_NAME='tblCalendarioEC_Coord' AND COLUMN_NAME <> 'RowID'";
                conn.Open();
                using (var reader = command.ExecuteReader())
                {

                    while (reader.Read())
                    {
                        //Agrega los tipos de datos de las columnas a una lista
                        types.Add(reader.GetString(0));

                    }
                }
                conn.Close();
            }
        }
        #endregion

        #region Verifica Null, puebla DataTable "tipos"

        //genera las columnas y filas en el DataTable "tipos"
        foreach (DataColumn col in dt.Columns)
        {
            //Copia las columnas de "dt" dentro de "tipos"
            tipos.Columns.Add(col.ColumnName);
        }
        foreach (DataRow r in dt.Rows)
        {
            //Agrega una fila a "tipos" por cada fila que tiene "dt"
            tipos.Rows.Add();
        }

        //Recorre el DataTable "dt" para verificar por valores nulos y carga en la DataTable "tipos" el tipo de datos del archivo
        foreach (DataColumn col in dt.Columns)
        {
            //Recorre las filas de "dt"
            foreach (DataRow rows in dt.Rows)
            {
                object value = rows[col.ColumnName];
                if (value == DBNull.Value)//checa si el evalor es nulo
                {
                    //Crea coordenadas (x,y) que apuntan a una celda con un valor nulo
                    x1.Add(dt.Columns.IndexOf(col));
                    y1.Add(dt.Rows.IndexOf(rows));

                    e++;//Aumenta en uno el contador de nulos

                    //puebla la tabla "tipos" con el tipo de dato de cada celda que esta nulo
                    string t = dt.Rows[dt.Rows.IndexOf(rows)][dt.Columns.IndexOf(col)].GetType().ToString();
                    tipos.Rows[dt.Rows.IndexOf(rows)][dt.Columns.IndexOf(col)] = t;
                }
                else
                {
                    //puebla la tabla "tipos" con el tipo de dato de cada celda que no este nulo
                    string t = dt.Rows[dt.Rows.IndexOf(rows)][dt.Columns.IndexOf(col)].GetType().ToString();
                    tipos.Rows[dt.Rows.IndexOf(rows)][dt.Columns.IndexOf(col)] = t;
                }
            }
        }
        #endregion

        #region Verifica que los tipos de datos sean iguales
        foreach (DataColumn col in tipos.Columns)
        {
            foreach (DataRow row in tipos.Rows)
            {
                string evaluar = tipos.Rows[tipos.Rows.IndexOf(row)][tipos.Columns.IndexOf(col)].ToString();
                if (!tp[evaluar].Equals(types[tipos.Columns.IndexOf(col)]))//Verifica si el tipo de datos no son iguales
                {

                        x2.Add(tipos.Columns.IndexOf(col));
                        y2.Add(tipos.Rows.IndexOf(row));
                        e++;
                }
                else
                {

                }
            }
        }
        #endregion

        //Checa si hubo errores, si los hubo descarga un excel con las celdas con error marcadas, si no procede a cargar los datos a la BD
        #region Checa si hubo errores
        if (e == 0)
        {
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert", "alert('Mira ama, sin errores!!');", true);
            CargarDB(dt);
        }
        else
        {
            e = 0;
            CorrecionesExcel(ds, x1, y1, x2, y2);
        }
        #endregion

        #region DataGrids
        GridView1.DataSource = dt;
        GridView1.DataBind();
        #endregion

    }

    //Checa por el nombre del project, si existe: set Id = Max IdVisitas where Calendario = project; else Id=1
    public int VerificarId(string project)
    {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString))
        {
            string sql = "SELECT DISTINCT 1 AS Existe FROM [DWH].[dbo].[tblCalendarioEC_Coord] WHERE Calendario = @project";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("@project", SqlDbType.NVarChar);
            cmd.Parameters["@project"].Value = project;
            conn.Open();
            object o = cmd.ExecuteScalar();
            conn.Close();
            if (o != null)
            {
                using (SqlConnection conn3 = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString))
                {
                    string sql3 = "SELECT [ID Visita] FROM [DWH].[dbo].[tblCalendarioEC_Coord] where [RowID] = (select MAX([RowId]) from [DWH].[dbo].[tblCalendarioEC_Coord]) AND Calendario = @project";
                    SqlCommand cmd3 = new SqlCommand(sql3, conn3);
                    cmd3.CommandType = CommandType.Text;
                    conn3.Open();
                    object o3 = cmd3.ExecuteScalar();
                    id = Convert.ToInt32(o3.ToString()) + 1;
                    conn3.Close();
                }
            }
            else
            {
                id = 1;
            }
        }
        return id;
    }

    //Crea un archivo de excel que se descarga automaticamente, este tiene marcadas las celdas que tienen errores
    public void CorrecionesExcel(DataSet ds, List<int> x1, List<int> y1, List<int> x2, List<int> y2)
    {
        DataTable dt = ds.Tables[0];
        DataTable tipos = ds.Tables[1];
        dt.TableName = "Hoja1";
        using (XLWorkbook libro = new XLWorkbook())
        {
            IXLWorksheet hoja = libro.Worksheets.Add(dt);
            hoja.ColumnsUsed().AdjustToContents();
            hoja.Rows().Hide();
            hoja.FirstRowUsed().Unhide();
            for (int i = 0; i < x1.Count; i++)
            {
                hoja.Row(y1[i] + 2).Unhide();
                hoja.Cell(y1[i] + 2, x1[i] + 1).Style.Fill.BackgroundColor = XLColor.Red;
            }
            for (int i = 0; i < x2.Count; i++)
            {
                hoja.Row(y2[i] + 2).Unhide();
                hoja.Cell(y2[i] + 2, x2[i] + 1).Style.Fill.BackgroundColor = XLColor.Blue;
            }
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=Correciones" + DateTime.Now + ".xlsx");
            using (MemoryStream stream = new MemoryStream())
            {
                libro.SaveAs(stream);
                stream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
        }
    }

    #endregion

    #region Carga en BD

    //Una vez validado todo revisa el nombre de las columnas en la BD y las compara con el del archivo, mapea aquellas que sean iguales
    //Manda los datos del archivo a la BD de las columnas mapeadas
    public void CargarDB(DataTable dt)
    {
        int success = 0;
        List<String> col = new List<string>();//Guarda el nombre de las columnas de la BD
        //Prepara conexion a BD y objeto que importara los datos de "dt"
        try
        {
            SqlConnection c = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString);
            SqlBulkCopy s = new SqlBulkCopy(c);
            s.DestinationTableName = "tblCalendarioEC_Coord";//Selecciona la tabla de destino

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString))
            {
                using (SqlCommand command = conn.CreateCommand())
                {
                    //Lee los nombres de las columnas
                    command.CommandText = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS  WHERE COLUMN_NAME <> 'RowID' ORDER BY ORDINAL_POSITION";
                    conn.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            //Agrega los nombre de las columnas a una lista
                            col.Add(reader.GetString(0));
                        }
                    }
                    conn.Close();
                }
            }

            int t = col.Count;//Obtiene el numero de columnas
            for (int i = 0; i < t; i++)
            {
                //Asigna el nombre de las columnas origen y destino en relación al nombre de las columnas en la BD   que se guardaron en la lista col             
                s.ColumnMappings.Add(col[i], col[i]);
            }
            s.BulkCopyTimeout = 0;
            c.Open();
            s.WriteToServer(dt);
            col.Clear();
            c.Close();
            success = 1;
        }
        catch
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert", "alert('Algo paso papu :O, no se que fue pero no se hizo la cargada');", true);
        }

        if (success == 1)
        {
            success = 0;
            CorrerSP(dt);
        }
        else
        {
            success = 0;
        }
    }

    //Ejecuta un Stored Procedure (spProyectos) y manda los respectivos parametros, el resto de Stored Procedures estan encadenados asi solo se debe ejecutar el primero desde aqui
    public void CorrerSP(DataTable dt)
    {
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();

        
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert", "alert("+fInicial+");", true);

        #region spProyectos
        try
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["con2"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "DaSPBro";
                cmd.Connection = conn;
                cmd.Parameters.AddWithValue("@MES", month1);//se listo para no dejarlo en manos del usuario                
                cmd.Parameters.AddWithValue("@Anio", year1);//se podria asignar automatico pero a fin de año podria provocar inconvenientes
                cmd.Parameters.AddWithValue("@NombreProyecto", project);//no dejarlo a manos del usuario seria lo mejor
                cmd.Parameters.AddWithValue("@IdVisita", id);// if si es nuevo se asigna uno si no hay quebuscar el ultimo
                cmd.Parameters.AddWithValue("@IDCliente", 160);//confirmar con usuario que cliente es al que corresponde dicho id seria lo mejor
                cmd.Parameters.AddWithValue("@FechaIni", fInicial);//se incluyo date picker para seleccionar el dia de inicio, talvez se podria limitar mas en dropdown list
                cmd.Parameters.AddWithValue("@Equipo", service1);//si se puede listar para no dejarlo a manos del usuario mejor
                conn.Open();
                int a = cmd.ExecuteNonQuery();
                string s = "DaSPBro: " + a.ToString();
                sb.Append(s);
                conn.Close();
            }
        }
        catch
        {
            
            sb2.Append("Fallo DaSPBro ");
        }
        #endregion

        Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert", "alert("+sb.ToString()+");", true);
        Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert", "alert(" + sb2.ToString() + ");", true);
    }

    #endregion
}

//Apartir de aqui dejo caer lineas de codigo que siempre no use pero no quiero olvidar su logica, talvez las borre al final
//Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert", "alert('columna <b>" + x[i] + "</b>, fila <b>" + y[i] + "</b>');", true);
//sb.Append("[" + dt.Columns.IndexOf(col).ToString() + "]" + "[" + dt.Rows.IndexOf(rows).ToString() + "]" + ",");
//string tipo = dt.Rows[1][1].ToString();
//TextBox1.Text = tipo;
//string t = dt.Rows[1][1].GetType().ToString();
//TextBox2.Text = t;
//var returnParameter = cmd.Parameters.Add("@ReturnVal", SqlDbType.Int);
//returnParameter.Direction = ParameterDirection.ReturnValue;
//var result = returnParameter.Value;
//int a = Convert.ToInt32(result);
//string b = a.ToString();
//Button3.Text = b;

