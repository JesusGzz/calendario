﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" MasterPageFile="MasterPage.master" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/jquery-1.9.1.min.js"></script>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="BodyContent" runat="server">

    <div>
        <asp:FileUpload ID="FileUpload1" runat="server" />
        
    </div>
    <div>
        <asp:Button ID="Button1" runat="server" Text="Cargar Excel" OnClick="CargarExcel" ValidationGroup="ValidarArchivo" />
    </div>


    <div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="true"></asp:GridView>
    </div>

    
    <script>

    </script>
</asp:Content>
